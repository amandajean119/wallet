class Category < ActiveRecord::Base
  has_many :transactions, :dependent => :delete_all
  belongs_to :user
  validates :user, presence: true
end
