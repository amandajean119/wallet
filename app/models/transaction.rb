class Transaction < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  validates_presence_of :description, :user, :amount, :category
  enum ttype: [ :cost, :income ]
end
