json.array!(@transactions) do |transaction|
  json.extract! transaction, :id, :description, :amount, :ttype, :category_id, :user_id
  json.url transaction_url(transaction, format: :json)
end
