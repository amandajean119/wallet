class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :amount
      t.integer :ttype
      t.references :category, index: true, foreign_key: true
    end
  end
end
